import os
import tempfile
import subprocess
import tkinter as tk
from tkinter import filedialog
import pygubu


PROJECT_PATH = os.path.dirname(__file__)
PROJECT_UI = os.path.join(PROJECT_PATH, "main.ui")
PROJECT_IMAGES = os.path.join(PROJECT_PATH, 'images')


class MainApp:
    def __init__(self):
        self.builder = builder = pygubu.Builder()
        builder.add_resource_path(PROJECT_IMAGES)
        builder.add_from_file(PROJECT_UI)
        self.mainwindow = builder.get_object('toplevel1')
        builder.connect_callbacks(self)
        
        self.txt = builder.get_object('text1')
        self.canvas = builder.get_object('canvas1')
        self.image_id = self.canvas.create_image(5,5, anchor='nw')
        self.image = None
        
        self.file_dialog_options = {
            'defaultextension': '.diag',
            'filetypes': (('diagram files', '*.diag'), ('All', '*.*'))
        }
    
    def getText(self):
        return self.txt.get('0.0', 'end')
    
    def setText(self, text):
        self.txt.delete('0.0', 'end')
        self.txt.insert('0.0', text)
        
    def on_update(self):
        with tempfile.NamedTemporaryFile() as ifile:
            txt = self.getText()
            ifile.write(txt.encode('utf-8'))
            ifile.flush()
            ofile = tempfile.NamedTemporaryFile(delete=False)
            ofile.close()
            args = ['nwdiag', '-T', 'png', '-o', ofile.name, ifile.name]
            try:
                subprocess.run(args, check=True)
                self.image = tk.PhotoImage(file=ofile.name)
                self.canvas.itemconfigure(self.image_id, image=self.image)
                self.canvas.configure(scrollregion=self.canvas.bbox("all"))
                os.unlink(ofile.name)
            except subprocess.CalledProcessError as e:
                print(e)

    def on_open(self):
        filename = filedialog.askopenfilename(**self.file_dialog_options)
        if filename:
            with open(filename) as ifile:
                txt = ifile.read()
                self.setText(txt)

    def on_save(self):
        filename = filedialog.asksaveasfilename(**self.file_dialog_options)
        if filename:
            with open(filename, 'w') as ofile:
                txt = self.getText()
                ofile.write(txt)

    def run(self):
        self.mainwindow.mainloop()

if __name__ == '__main__':
    app = MainApp()
    app.run()

